<?php

use yupe\components\WebModule;

class TimelineModule extends WebModule
{

    const VERSION = '0.1.1';
    public $uploadPath = 'timeline';
    public $assetsPath = 'application.modules.timeline.views.assets';

    public $allowedExtensions = 'jpg,jpeg,png,gif';
    public $minSize = 1;
    public $maxSize = 5368709120;
    public $maxFiles = 1;

    public $phoneMask = '+7-999-999-99-99';
    public $phonePattern = '/^((\+?7)(-?\d{3})-?)?(\d{3})(-?\d{2})(-?\d{2})$/';
    //public $phonePattern = '/^((\+?7|8)(?!95[4-79]|99[08]|907|94[^0]|336|986)([348]\d|9[0-6789]|7[0247])\d{8}|\+?(99[^4568]\d{7,11}|994\d{9}|9955\d{8}|996[57]\d{8}|9989\d{8}|380[34569]\d{8}|375[234]\d{8}|372\d{7,8}|37[0-4]\d{8}))$/';
    //public $phonePattern = '/(8|7|\+7){0,1}[- \\\\(]{0,}([9][0-9]{2})[- \\\\)]{0,}(([0-9]{2}[- ]{0,}[0-9]{2}[- ]{0,}[0-9]{3})|([0-9]{3}[- ]{0,}[0-9]{2}[- ]{0,}[0-9]{2})|([0-9]{3}[- ]{0,}[0-9]{1}[- ]{0,}[0-9]{3})|([0-9]{2}[- ]{0,}[0-9]{3}[- ]{0,}[0-9]{2}))/';
    public $generateNickName = 1;
    public $generateEmail = 1;


    public function getDependencies()
    {
        return [
            'user'
        ];
    }

    /**
     * @return string
     */
    public function getVersion()
    {
        return self::VERSION;
    }
    /**
     * @return string
     */
    public function getUploadPath()
    {
        return Yii::getPathOfAlias('webroot') . '/' . Yii::app()->getModule('yupe')->uploadPath . '/' . $this->uploadPath;
    }

    /**
     * @return bool
     */
    public function getInstall()
    {
        if (parent::getInstall()) {
            @mkdir($this->getUploadPath(), 0755);
        }

        return true;
    }

    /**
     * @return bool
     */
    public function getIsInstallDefault()
    {
        return false;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return Yii::t('TimelineModule.timeline', 'Timeline');
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return Yii::t('TimelineModule.timeline', 'Module for managing Timeline');
    }

    /**
     * @return string
     */
    public function getAuthor()
    {
        return Yii::t('TimelineModule.timeline', 'Maxim Avramenko');
    }

    /**
     * @return string
     */
    public function getAuthorEmail()
    {
        return Yii::t('TimelineModule.timeline', 'maxim.avramenko@gmail.com');
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return Yii::t('TimelineModule.timeline', 'http://afclinic.ru');
    }

    /**
     * @return string
     */
    public function getCategory()
    {
        return Yii::t('TimelineModule.timeline', 'Timeline');
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'glyphicon glyphicon-globe';
    }

    public function getAdminPageLink()
    {
        return '/backend/timeline/timelineEvent/index';
    }

    /**
     * @return array menu navigation
     */
    public function getNavigation()
    {
        return [
            ['label' => Yii::t('TimelineModule.timeline', 'Графики')],
            [
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('TimelineModule.timeline', 'Сводный график'),
                'url'   => ['/backend/timeline/timelineDoctor/index']
            ],
            [
                'icon'  => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('TimelineModule.timeline', 'Добавить специалиста'),
                'url'   => ['/backend/timeline/timelineDoctor/create']
            ],
            ['label' => Yii::t('TimelineModule.timeline', 'Отчеты PDF')],
            [
                'icon'  => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('TimelineModule.timeline', 'Пациенты по врачам на сегодня'),
                'url'   => ['/backend/timeline/timelineDoctor/scheduleToday']
            ],
        ];
    }

    /**
     * статус работы мультиязычности в модуле
     *
     * @return bool
     */
    public function isMultiLang()
    {
        return false;
    }

    /**
     * @return array|bool
     */
    public function checkSelf()
    {
        $messages = array();

        $uploadPath = Yii::app()->uploadManager->getBasePath() . DIRECTORY_SEPARATOR . $this->uploadPath;

        if (!is_writable($uploadPath)) {
            $messages[WebModule::CHECK_ERROR][] = array(
                'type' => WebModule::CHECK_ERROR,
                'message' => Yii::t(
                    'TimelineModule.timeline',
                    'Directory "{dir}" is not accessible for write! {link}',
                    array(
                        '{dir}' => $uploadPath,
                        '{link}' => CHtml::link(
                            Yii::t('TimelineModule.timeline', 'Change settings'),
                            array(
                                '/yupe/backend/modulesettings/',
                                'module' => 'timeline',
                            )
                        ),
                    )
                ),
            );
        }

        return (isset($messages[WebModule::CHECK_ERROR])) ? $messages : true;
    }



	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'timeline.models.*',
			'timeline.components.*',
		));
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}

/*    public function getAuthItems()
    {
        return [];
    }*/
}
