<?php
return [
    '*unknown*' => '*не указан*',
    '--choose--' => '--выберите--',
    '--no--' => '--нет--',
    'Timeline' => 'Расписание',
    'Manage Timeline' => 'Управление Расписанием',
    'Create Timeline' => 'Создать Событие',
    'Update Timeline' => 'Обновить Событие',
    'http://afclinic.ru' => 'http://afclinic.ru',
    'maxim.avramenko@gmail.com' => 'maxim.avramenko@gmail.com',
    'Maxim Avramenko' => 'Максим Авраменко',
    'Module for managing Timeline' => 'Расписание врачей',
    'Directory "{dir}" is not accessible for write! {link}' => 'Директория "{dir}" не доступна для записи! {link}',
    '' => '',
];