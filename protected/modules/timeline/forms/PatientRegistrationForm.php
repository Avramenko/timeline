<?php

/**
 * Форма регистрации
 *
 * @category YupeComponents
 * @package  yupe.modules.user.forms
 * @author   YupeTeam <team@yupe.ru>
 * @license  BSD http://ru.wikipedia.org/wiki/%D0%9B%D0%B8%D1%86%D0%B5%D0%BD%D0%B7%D0%B8%D1%8F_BSD
 * @version  0.5.3
 * @link     http://yupe.ru
 *
 **/
class PatientRegistrationForm extends CFormModel
{
    /**
     *
     */
    const GENDER_THING = 0;
    /**
     *
     */
    const GENDER_MALE = 1;
    /**
     *
     */
    const GENDER_FEMALE = 2;

    public $nick_name;
    public $email;
    public $first_name;
    public $middle_name;
    public $last_name;
    public $gender;
    public $birth_date;
    public $no_email;


    public function rules()
    {
        //$module = Yii::app()->getModule('user');

        return [
            ['nick_name, email, first_name, middle_name, last_name', 'filter', 'filter' => 'trim'],
            ['nick_name, email, first_name, middle_name, last_name', 'filter', 'filter' => [new CHtmlPurifier(), 'purify']],
            ['nick_name', 'required'],
            ['nick_name', 'length', 'max' => 11],
            ['nick_name', 'length', 'min' => 11],
            ['email, first_name, middle_name, last_name', 'length', 'max' => 50],
            [
                'nick_name',
                'match',
                'pattern' => '/^[0-9]{11,11}$/',
                'message' => Yii::t(
                    'TimelineModule.timeline',
                    'Bad field format for "{attribute}". You can use only digits and only 11 symbols'
                )
            ],
            ['nick_name', 'checkNickName'],
            ['email', 'email'],
            ['email', 'checkEmail'],
            ['email', 'default', 'setOnEmpty' => true, 'value' => 'nick_name'],
            ['birth_date', 'default', 'setOnEmpty' => true, 'value' => null],
        ];
    }

    public function attributeLabels()
    {
        return [
            'nick_name'  => Yii::t('TimelineModule.timeline', 'Телефон'),
            'email'      => Yii::t('TimelineModule.timeline', 'Email'),
            'last_name'      => Yii::t('TimelineModule.timeline', 'Фамилия'),
            'first_name'      => Yii::t('TimelineModule.timeline', 'Имя'),
            'middle_name'      => Yii::t('TimelineModule.timeline', 'Отчество'),
            'gender' => Yii::t('TimelineModule.timeline', 'Пол'),
            'birth_date' => Yii::t('UserModule.user', 'Дата рождения'),
        ];
    }

    public function checkNickName($attribute, $params)
    {
        $model = User::model()->find('nick_name = :nick_name', [':nick_name' => $this->$attribute]);

        if ($model) {
            $this->addError('nick_name', Yii::t('TimelineModule.timeline', 'User name already exists'));
        }
    }

    public function checkEmail($attribute, $params)
    {
        $model = User::model()->find('email = :email', [':email' => $this->$attribute]);

        if ($model) {
            $this->addError('email', Yii::t('TimelineModule.timeline', 'Email already busy'));
        }
    }

    /**
     * Список статусов половой принадлежности:
     *
     * @return array
     */
    public function getGendersList()
    {
        return [
            self::GENDER_FEMALE => Yii::t('UserModule.user', 'female'),
            self::GENDER_MALE => Yii::t('UserModule.user', 'male'),
            self::GENDER_THING => Yii::t('UserModule.user', 'not set'),
        ];
    }

    /**
     * Получаем строковое значение половой
     * принадлежности пользователя:
     *
     * @return string
     */
    public function getGender()
    {
        $data = $this->getGendersList();

        return isset($data[$this->gender])
            ? $data[$this->gender]
            : $data[self::GENDER_THING];
    }

    /*public function getStatus()
    {
        return 1;
    }


    public function getAccess_level()
    {
        return 0;
    }

    public function getEmail_confirm()
    {
        return 1;
    }

    public function getEmail()
    {
        $time = microtime(true);
        return microtime(true).''.$time.'.ru';
    }*/
}
