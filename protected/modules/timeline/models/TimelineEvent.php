<?php

/**
 * This is the model class for table "{{timeline_event}}".
 *
 * The followings are the available columns in table '{{timeline_event}}':
 * @property integer $id
 * @property integer $doctor_id
 * @property integer $patient_id
 * @property integer $visit
 * @property string $comment
 * @property string $description
 * @property string $source
 * @property integer $status
 * @property integer $notified
 * @property integer $create_user_id
 * @property integer $update_user_id
 * @property string $start_time
 * @property string $end_time
 * @property string $create_time
 * @property string $update_time
 *
 * The followings are the available model relations:
 * @property User $updateUser
 * @property User $createUser
 * @property TimelineDoctor $doctor
 * @property User $patient
 * @property TimelineMessage[] $messages
 */
class TimelineEvent extends \yupe\models\YModel
{
    /**
     *
     */
    const NOTIFIED_FALSE = 0;

    /**
     *
     */
    const NOTIFIED_TRUE = 1;

    /**
     *
     */
    const STATUS_NOT_ACTIVE = 0;

    /**
     *
     */
    const STATUS_ACTIVE = 1;

    /**
     *
     */
    const VISIT_NOT_NEW = 2;
    const VISIT_NEW = 1;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{timeline_event}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('doctor_id', 'required'),
			array('patient_id', 'required','except'=>'createWithPatient, updateWithPatient'),
			array('doctor_id, patient_id, visit, status, notified, create_user_id, update_user_id', 'numerical', 'integerOnly'=>true),
			array('source', 'length', 'max'=>255),
			array('comment, description, start_time, end_time, create_time, update_time', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, doctor_id, patient_id, visit, comment, description, source, status, notified, create_user_id, update_user_id, start_time, end_time, create_time, update_time', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'updateUser' => array(self::BELONGS_TO, 'User', 'update_user_id'),
			'createUser' => array(self::BELONGS_TO, 'User', 'create_user_id'),
			'doctor' => array(self::BELONGS_TO, 'TimelineDoctor', 'doctor_id'),
			'patient' => array(self::BELONGS_TO, 'User', 'patient_id'),
			'messages' => array(self::MANY_MANY, 'TimelineMessage', '{{timeline_event_to_message}}(event_id, message_id)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'doctor_id' => 'Доктор',
			'patient_id' => 'Пациент',
			'visit' => 'Визит',
			'comment' => 'Комментарий',
			'description' => 'Описание',
			'source' => 'Направление от',
			'status' => 'Статус',
			'notified' => 'Оповещен',
			'create_user_id' => 'Создал',
			'update_user_id' => 'Обновил',
			'start_time' => 'Дата начала',
			'end_time' => 'Дата Окончания',
			'create_time' => 'Дата создания',
			'update_time' => 'Дата обновления',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('doctor_id',$this->doctor_id);
		$criteria->compare('patient_id',$this->patient_id);
		$criteria->compare('visit',$this->visit);
		$criteria->compare('comment',$this->comment,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('source',$this->source,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('notified',$this->notified);
		$criteria->compare('create_user_id',$this->create_user_id);
		$criteria->compare('update_user_id',$this->update_user_id);
		$criteria->compare('start_time',$this->start_time,true);
		$criteria->compare('end_time',$this->end_time,true);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('update_time',$this->update_time,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TimelineEvent the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    /**
     * @return array
     */
    public function behaviors()
    {
        if(PHP_SAPI=='cli')
            return [
                'CTimestampBehavior' => [
                    'class'             => 'zii.behaviors.CTimestampBehavior',
                    'setUpdateOnCreate' => true,
                ],
            ];

        //$module = Yii::app()->getModule('broadcast');
        return [
            'CTimestampBehavior' => [
                'class'             => 'zii.behaviors.CTimestampBehavior',
                'setUpdateOnCreate' => true,
            ],
        ];
    }

    /**
     * @return array
     */
    public function scopes()
    {
        return [
            'active' => [
                'condition' => 't.status = :status',
                'params'    => [':status' => self::STATUS_ACTIVE],
            ],
            'notactive'    => [
                'condition' => 't.status = :status',
                'params'    => [':status' => self::STATUS_NOT_ACTIVE],
            ],
        ];
    }

    public function period($start_time,$end_time,$status)
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition' =>'events.status =:status AND start_time >=:start_time AND end_time <=:end_time',
            'params' => [
                'start_time' => $start_time,
                'end_time' => $end_time,
                'status' => $status

            ]
        ));
        return $this;
    }
    /**
     * Список возможных статусов пользователя:
     *
     * @return array
     */
    public function getStatusList()
    {
        return [
            self::STATUS_ACTIVE => Yii::t('TimelineModule.timeline', 'Назначено'),
            self::STATUS_NOT_ACTIVE => Yii::t('TimelineModule.timeline', 'Отменено'),
        ];
    }

    /**
     * Получение строкового значения
     * статуса пользователя:
     *
     * @return string
     */
    public function getStatus()
    {
        $data = $this->getStatusList();

        return isset($data[$this->status])
            ? $data[$this->status]
            : Yii::t('TimelineModule.timeline', 'status is not set');
    }
    /**
     * Список возможных статусов пользователя:
     *
     * @return array
     */
    public function getNotifiedList()
    {
        return [
            self::NOTIFIED_FALSE => Yii::t('TimelineModule.timeline', 'Не оповещен'),
            self::NOTIFIED_TRUE => Yii::t('TimelineModule.timeline', 'Оповещен'),
        ];
    }

    /**
     * Получение строкового значения
     * статуса пользователя:
     *
     * @return string
     */
    public function getNotified()
    {
        $data = $this->getNotifiedList();

        return isset($data[$this->notified])
            ? $data[$this->notified]
            : Yii::t('TimelineModule.timeline', 'notified is not set');
    }


    /**
     * Список возможных статусов пользователя:
     *
     * @return array
     */
    public function getVisitList()
    {
        return [
            self::VISIT_NOT_NEW => Yii::t('TimelineModule.timeline', 'Повторный'),
            self::VISIT_NEW => Yii::t('TimelineModule.timeline', 'Первый'),
        ];
    }

    /**
     * Получение строкового значения
     * статуса пользователя:
     *
     * @return string
     */
    public function getVisit()
    {
        $data = $this->getVisitList();

        return isset($data[$this->visit])
            ? $data[$this->visit]
            : Yii::t('TimelineModule.timeline', 'visit type is not set');
    }


    /**
     * @return bool
     */
    public function beforeSave()
    {
        if($this->getIsNewRecord()){
            $this->create_user_id = Yii::app()->user->id;
            $this->status = self::STATUS_ACTIVE;
            $this->notified = self::NOTIFIED_FALSE;
        }else{
            $this->update_user_id = Yii::app()->user->id;
        }



        return parent::beforeSave();
    }

    /**
     * @throws CDbException
     */
    public function toArchive()
    {
        $this->status = self::STATUS_NOT_ACTIVE;
        return $this->update(array('status'));
    }







    public function getCalendarJsObject()
    {
        return CJSON::encode(array(
            'id' => $this->id,
            'resourceId' => $this->doctor->id,
            'start' => $this->start_time,
            'end' => $this->end_time,
            'title' => (strlen($this->patient->about) ? $this->patient->getFullName() .' ('.$this->patient->about.')' : $this->patient->getFullName()),
            //'className' => 'tooltip',
            'url' => Yii::app()->createUrl('/backend/timeline/timelineEvent/view/'.$this->id),
            'color' => $this->doctor->color
        ));
    }




    public function getEventJson()
    {
        return CJSON::encode([
            'id' => $this->id,
            'resourceId' => $this->doctor->id,
            'start' => $this->start_time,
            'end' => $this->end_time,
            'title' => (strlen($this->patient->about) ? $this->patient->getFullName() .' ('.$this->patient->about.')' : $this->patient->getFullName()),
            //'className' => 'tooltip',
            'url' => Yii::app()->createUrl('/backend/timeline/timelineEvent/view/'.$this->id),
            'color' => $this->doctor->color
        ]);
    }

    public function getArray($for = null)
    {
        $arr = [
            'id' => $this->id,
            'start' => $this->start_time,
            'end' => $this->end_time,
            'title' => (strlen($this->patient->about) ? $this->patient->getFullName() .' ('.$this->patient->about.')' : $this->patient->getFullName()),
            'constraint' => 'available',
            'color' => $this->doctor->color
        ];
        return $arr;
    }

    /**
     * @return string продолжительность визита в часах и минутах "00 ч. 15 мин."
     */
    public function getEventDuration($format)
    {
        $datetime1 = new DateTime($this->start_time);
        $datetime2 = new DateTime($this->end_time);
        $interval = $datetime1->diff($datetime2);
        return $interval->format($format);
        //return true;
    }

}
