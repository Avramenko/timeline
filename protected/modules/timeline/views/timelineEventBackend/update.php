<?php
/**
 * Отображение для update:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
    $this->breadcrumbs = array(
        Yii::app()->getModule('timeline')->getCategory() => array(),
        Yii::t('timeline', 'События') => array('/backend/timeline/timelineEvent/index'),
        $model->id => array('/backend/timeline/timelineEvent/view', 'id' => $model->id),
        Yii::t('timeline', 'Редактирование'),
    );

    $this->pageTitle = Yii::t('timeline', 'События - редактирование');

    $this->menu = array(
        array('icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('timeline', 'Управление Событиями'), 'url' => array('/backend/timeline/timelineEvent/index')),
        array('icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('timeline', 'Добавить Событие'), 'url' => array('/backend/timeline/timelineEvent/create')),
        array('label' => Yii::t('timeline', 'Событие') . ' «' . mb_substr($model->id, 0, 32) . '»'),
        array('icon' => 'fa fa-fw fa-pencil', 'label' => Yii::t('timeline', 'Редактирование События'), 'url' => array(
            '/backend/timeline/timelineEvent/update',
            'id' => $model->id
        )),
        array('icon' => 'fa fa-fw fa-eye', 'label' => Yii::t('timeline', 'Просмотреть Событие'), 'url' => array(
            '/backend/timeline/timelineEvent/view',
            'id' => $model->id
        )),
        array('icon' => 'fa fa-fw fa-trash-o', 'label' => Yii::t('timeline', 'Удалить Событие'), 'url' => '#', 'linkOptions' => array(
            'submit' => array('/backend/timeline/timelineEvent/delete', 'id' => $model->id),
            'confirm' => Yii::t('timeline', 'Вы уверены, что хотите удалить Событие?'),
            'csrf' => true,
        )),
    );
    $addToArchiveUrl = CHtml::normalizeUrl(['timelineEventBackend/addToArchive', 'id'=>$model->id]);
?>
    <div class="row">
        <div class="col-sm-12">
            <?php $box = $this->beginWidget(
                'booster.widgets.TbPanel',
                array(
                    'title' => 'Визит',
                    'context' => 'success',
                    'headerIcon' => 'th-list',
                    'padContent' => false,
                    'htmlOptions' => array('class' => 'bootstrap-widget-table'),
                    'headerButtons' => array(
                        array(
                            'class' => 'booster.widgets.TbButtonGroup',
                            'context' => 'success', // '', 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                            'buttons' => array(
                                //array('label' => ' Действия', 'url' => '#'), // this makes it split :)
                                array('label' => ' Действия'), // this makes it split :)
                                array(
                                    'items' => array(
                                        //array('label' => 'Пациент оповещен', 'url' => '#'),
                                        //array('label' => 'Оповестить СМС', 'url' => '#'),
                                        //array('label' => 'Оповестить голосом', 'url' => '#'),
                                        //'---',
                                        array(
                                            'label' => 'Отменить визит',
                                            'url' => '#',
                                            'itemOptions' => [
                                                'id' => 'discard-event',
                                                'data-discard-url'=>'/backend/timeline/timelineEvent/discard/'.$model->id,
                                                'onclick' => "js:bootbox.confirm('Вы действительно хотите отменить визит пациента?', function(confirmed){
                                                        if(confirmed){
                                                            // Меняем статус визита пациента
                                                            $.ajax({
                                                                type : 'post',
                                                                data : {".Yii::app()->getRequest()->csrfTokenName." : '".Yii::app()->getRequest()->csrfToken."'},
                                                                url : '".CHtml::normalizeUrl(['timelineEventBackend/addToArchive', 'id'=>$model->id])."',
                                                                success : function(data,status){
                                                                    if(data.status){
                                                                        $('#calendar').fullCalendar('refetchEvents');
                                                                        $('#my-modal').modal('hide');

                                                                    }
                                                                },
                                                                error : function(){
                                                                    bootbox.alert('Ошибка: Визит пациента не отменен, возможно у Вас не достаточно прав для данной операции, обратитесь в службу поддержки.')
                                                                }
                                                            });
                                                            console.log(this);
                                                        }
                                                        console.log('Confirmed: '+confirmed);}
                                                    )"
                                            ]
                                        ),

                                    )
                                ),
                            )
                        ),
                        /*array(
                            'class' => 'booster.widgets.TbButtonGroup',
                            'buttons' => array(
                                array('label' => 'Left', 'url' => '#'),
                                array('label' => 'Middle', 'url' => '#'),
                                array('label' => 'Right', 'url' => '#')
                            ),
                        ),*/
                    )
                )
            );?>
            <div class="row">
                <div class="col-md-6">
                    <?php
                    $this->widget(
                        'booster.widgets.TbDetailView',
                        array(
                            'data' => $model,
                            'type' => 'condensed',
                            'attributes' => [
                                [
                                    'name' => 'patient_id',
                                    'value' => $model->patient->fullName
                                ],
                                [
                                    'name' => 'doctor_id',
                                    'value' => $model->doctor->fullName
                                ],
                                [
                                    'name' => 'patient.about',
                                    'value' => $model->patient->about
                                ],
                                [
                                    'name' => 'createUser',
                                    'label' => 'Создал',
                                    'value' => $model->createUser->fullName
                                ]
                                ,
                                [
                                    'name' => 'updateUser',
                                    'label' => 'Обновил',
                                    'value' => ($model->updateUser) ? $model->updateUser->fullName : '',
                                ]
                            ],
                        )
                    );
                    ?>
                </div>
                <div class="col-sm-6">
                    <?php
                    $this->widget(
                        'booster.widgets.TbDetailView',
                        array(
                            'data' => $model,
                            //'url' => $this->createUrl('/timeline/timelineEventBackend/inline'),
                            'type' => 'condensed',
                            'attributes' => [

                                [
                                    //'header' => 'Дата и время визита:',
                                    //'type'=>'raw',
                                    //'name' => 'Дата и время визита',
                                    'label' => 'Дата',
                                    //'value' => Yii::app()->dateFormatter->formatDateTime($model->start_time)
                                    'value' => Yii::app()->getDateFormatter()->format('dd.MM.yyyy',$model->start_time),
                                ],
                                [
                                    'label' => 'Время',
                                    'value' => Yii::app()->getDateFormatter()->format('HH:mm',$model->start_time),
                                ],
                                [
                                    'label' => 'Длительность',
                                    'value' => $model->getEventDuration('%H ч. %I мин.')
                                ],
                                [
                                    'label' => 'Создано',
                                    'value' => Yii::app()->dateFormatter->formatDateTime($model->create_time)
                                ]
                                ,
                                [
                                    'label' => 'Обновлено',
                                    'value' => ($model->update_time) ? Yii::app()->dateFormatter->formatDateTime($model->update_time) : '',
                                ],
                            ],
                        )
                    );
                    ?>
                </div>
            </div>
            <?php $this->endWidget(); ?>
        </div>
    </div>

<!--<div class="page-header">
    <h1>
        <?php /*echo Yii::t('timeline', 'Редактирование') . ' ' . Yii::t('timeline', 'События'); */?> # <?php /*echo $model->id; */?>
    </h1>
</div>-->
<?php $box = $this->beginWidget(
    'booster.widgets.TbPanel',
    array(
        'title' => 'Пациент',
        'context' => 'success',
        'headerIcon' => 'user',
        'padContent' => false,
        'htmlOptions' => array('class' => 'bootstrap-widget-table')
    )
);?>
    <style type="text/css">
        .nav-pills>li.active>a, .nav-pills>li.active>a:hover, .nav-pills>li.active>a:focus
        {
            background-color: #5CB85C;
        }
    </style>
<?php
$tabs = array(

    array(
        'label' => 'Изменить визит',
        'content' => $this->renderPartial('_form',['model' => $model], true),
        'itemOptions' => array('class' => 'btn btn-default success'),
        'active' => true,
    ),
    array(
        'label' => 'Новый пациент',
        'content' => $this->renderPartial('_with_new_user_form',['model' => $model, 'user' => $user], true),
        'itemOptions' => array('class' => 'btn btn-default success'),
        'active' => false,
        'encodeLabel'=>false,
    ),
    array(
        'label' => 'Помощь',
        'content' => 'Текст странички помощь',
        'itemOptions' => array('class' => 'btn btn-default success'),
        'active' => false,
    ),
);

$this->widget(
    'booster.widgets.TbTabs',
    array(
        'type' => 'pills',
        'justified' => true,
        'tabs' => $tabs
    )
);

?>
<?php $this->endWidget(); ?>
<?php
//CVarDumper::dump($model->scenario);