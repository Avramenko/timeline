<?php
/**
 * Отображение для create:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
    $this->breadcrumbs = array(
        Yii::app()->getModule('timeline')->getCategory() => array(),
        Yii::t('timeline', 'События') => array('/backend/timeline/timelineEvent/index'),
        Yii::t('timeline', 'Добавление'),
    );

    $this->pageTitle = Yii::t('timeline', 'События - добавление');

    $this->menu = array(
        array('icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('timeline', 'Управление Событиями'), 'url' => array('/backend/timeline/timelineEvent/index')),
        array('icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('timeline', 'Добавить Событие'), 'url' => array('/backend/timeline/timelineEvent/create')),
    );


?>
    <div class="row">
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-12">
                    <?php $box = $this->beginWidget(
                        'booster.widgets.TbPanel',
                        array(
                            'title' => 'Визит',
                            'context' => 'primary',
                            'headerIcon' => 'th-list',
                            'padContent' => false,
                            'htmlOptions' => array('class' => 'bootstrap-widget-table')
                        )
                    );?>

                    <?php
                    $this->widget(
                        'booster.widgets.TbDetailView',
                        array(
                            'data' => $model,
                            'type' => 'condensed',
                            'attributes' => [
                                [
                                    'name' => 'doctor_id',
                                    'value' => $model->doctor->fullName
                                ],
                                [
                                    'label' => 'Дата и время',
                                    'value' => Yii::app()->dateFormatter->formatDateTime($model->start_time)
                                ],
                                [
                                    'label' => 'Длительность',
                                    'value' => $model->getEventDuration('%H ч. %I мин.')
                                ],
                            ],
                        )
                    );
                    ?>

                    <?php $this->endWidget(); ?>

                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?php $box = $this->beginWidget(
                'booster.widgets.TbPanel',
                array(
                    'title' => 'Пациент',
                    'context' => 'primary',
                    'headerIcon' => 'th-list',
                    'padContent' => false,
                    'htmlOptions' => array('class' => 'bootstrap-widget-table')
                )
            );?>
            <?php
            $tabs = array(

                array(
                    'label' => 'Повторный визит',
                    'content' => $this->renderPartial('_form',['model' => $model], true),
                    'itemOptions' => array('class' => 'btn btn-default'),
                    'active' => true,
                ),
                array(
                    'label' => 'Новый пациент',
                    'content' => $this->renderPartial('_with_new_user_form',['model' => $model, 'user' => $user], true),
                    'itemOptions' => array('class' => 'btn btn-default'),
                    'active' => false,
                    'encodeLabel'=>false,
                ),
                array(
                    'label' => 'Помощь',
                    'content' => 'Текст странички помощь',
                    'itemOptions' => array('class' => 'btn btn-default'),
                    'active' => false,
                ),
            );

            $this->widget(
                'booster.widgets.TbTabs',
                array(
                    'type' => 'pills',
                    'justified' => true,
                    'tabs' => $tabs
                )
            );

            ?>
            <?php $this->endWidget(); ?>
        </div>
    </div>

<?php
