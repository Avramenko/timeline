<?php
/**
 * Отображение для view:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
    $this->breadcrumbs = array(
        Yii::app()->getModule('timeline')->getCategory() => array(),
        Yii::t('timeline', 'События') => array('/backend/timeline/timelineEvent/index'),
        $model->id,
    );

    $this->pageTitle = Yii::t('timeline', 'События - просмотр');

    $this->menu = array(
        array('icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('timeline', 'Управление Событиями'), 'url' => array('/backend/timeline/timelineEvent/index')),
        array('icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('timeline', 'Добавить Событие'), 'url' => array('/backend/timeline/timelineEvent/create')),
        array('label' => Yii::t('timeline', 'Событие') . ' «' . mb_substr($model->id, 0, 32) . '»'),
        array('icon' => 'fa fa-fw fa-pencil', 'label' => Yii::t('timeline', 'Редактирование События'), 'url' => array(
            '/backend/timeline/timelineEvent/update',
            'id' => $model->id
        )),
        array('icon' => 'fa fa-fw fa-eye', 'label' => Yii::t('timeline', 'Просмотреть Событие'), 'url' => array(
            '/backend/timeline/timelineEvent/view',
            'id' => $model->id
        )),
        array('icon' => 'fa fa-fw fa-trash-o', 'label' => Yii::t('timeline', 'Удалить Событие'), 'url' => '#', 'linkOptions' => array(
            'submit' => array('/backend/timeline/timelineEvent/delete', 'id' => $model->id),
            'confirm' => Yii::t('timeline', 'Вы уверены, что хотите удалить Событие?'),
            'csrf' => true,
        )),
    );
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('timeline', 'Просмотр') . ' ' . Yii::t('timeline', 'События'); ?> # <?php echo $model->id; ?>
    </h1>
</div>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
'data'       => $model,
'attributes' => array(
        //'id',
    [
        'name' => 'doctor_id',
        'value' => $model->doctor->fullName
    ],
        //'doctor_id',
        //'patient_id',
    [
        'name' => 'patient_id',
        'value' => $model->patient->fullName
    ],
        //'visit',
    [
        'name' => 'visit',
        'value' => $model->getVisit()
    ],
        'comment',
        //'description',
        //'source',
        //'status',
    [
        'name' => 'patient.about',
        'value' => $model->patient->about
    ],
    [
        'name' => 'status',
        'value' => $model->getStatus()
    ],
    [
        'name' => 'notified',
        'value' => $model->getNotified()
    ],
    /*[
        'name' => 'create_user_id',
        'value' => $model->createUser->getFullName()
    ],
    [
        'name' => 'update_user_id',
        'value' => $model->updateUser->getFullName()
    ],*/
    /*[
        'name' => 'create_user_id',
        'value' => $model->createUser->id
    ],*/
    /*[
        'name' => 'update_user_id',
        'value' => $model->updateUser->id
    ],*/
        'start_time',
        'end_time',
        'create_time',
        'update_time',
),
)); ?>
<a href="#" data-url="<?=CHtml::normalizeUrl(['timelineEventBackend/update', 'id'=>$model->id])?>" class="update-event-button btn btn-primary">Редактировать</a>
<?php
/*echo CHtml::ajaxLink(
    $text = 'Редактировать',
    $url = CHtml::normalizeUrl(['timelineEventBackend/update', 'id'=>$model->id]),
    $ajaxOptions=array (
        'type'=>'GET',
        'dataType'=>'html',
        'success'=>'function(data){
            //if(data.status){
                //$("#calendar").fullCalendar("refetchEvents");
                $("#view-event-modal .modal-body").html(data);
                //$("#view-event-modal").modal("hide");
            //}
            //console.log(html);
        }',
        'error' => 'js:function(data,status){
            console.log(data);
        }'

    ),
    $htmlOptions=array (
        'id' => 'update-event-'.$model->id,
        'class' => 'btn btn-primary pull-left '
    )
);*/
/*echo CHtml::ajaxLink(
    $text = 'Удалить',
    $url = CHtml::normalizeUrl(['timelineEventBackend/addToArchive', 'id'=>$model->id]),
    $ajaxOptions=array (
        'type'=>'POST',
        'dataType'=>'json',
        'data' => [
            Yii::app()->getRequest()->csrfTokenName => Yii::app()->getRequest()->csrfToken
        ],
        'success'=>'function(data){
            if(data.status){
                $("#calendar").fullCalendar("refetchEvents");
                $("#view-event-modal").modal("hide");
            }
            //console.log(html);
        }',
        'error' => 'js:function(data,status){
            console.log(data);
        }'

    ),
    $htmlOptions=array (
        'id' => 'archive-event-'.$model->id,
        'class' => 'btn btn-danger pull-right '
    )
);*/

?>
<!--<a href="#" id="remove-event" class="btn btn-danger pull-right" data-url="weqdwed">Удалить</a>-->
<div class="clearfix"></div>
