<?php
//CVarDumper::dump($model->getScenario());
$form = $this->beginWidget(
    'bootstrap.widgets.TbActiveForm',
    [
        'id'                     => 'new-user-timeline-event-form',
        'action'                 => ($model->getScenario() === 'update' ? CHtml::normalizeUrl(array("/backend/timeline/timelineEvent/updateWithPatient/".$model->id)) : CHtml::normalizeUrl(array("TimelineEventBackend/createWithPatient"))),
        'enableAjaxValidation'   => true,
        'enableClientValidation' => true,
        //'type'                   => 'vertical',
        'type'                   => 'horizontal',
        'htmlOptions'            => ['class' => 'well'],
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'afterValidate'=>'js:function(form,data,hasError){
                                    if(!hasError){
                                        $.ajax({
                                            type:"POST",

                                            url:"'.($model->getScenario() === 'update' ? CHtml::normalizeUrl(array("/backend/timeline/timelineEvent/updateWithPatient/".$model->id)) : CHtml::normalizeUrl(array("TimelineEventBackend/createWithPatient"))).'",
                                            data:form.serialize(),
                                            dataType : "json",
                                            success: function(d,status){
                                                if(status){
                                                    //$("#calendar").fullCalendar("refetchEvents");
                                                    //bootbox.alert("Новый пациент "+d.model.last_name+" "+d.model.first_name+" "+d.model.middle_name+" успешно создан! Для продолжения нажмите кнопку \'ОК\'")
                                                    $("#calendar").fullCalendar("refetchEvents");
                                                    $("#my-modal").modal("hide");
                                                    //$("#my-modal").modal("hide");

                                                }
                                            }
                                        });
                                    }
                                }'
        ),
    ]
);
?>

<div class="alert alert-info">
    <?php echo Yii::t('timeline', 'Поля, отмеченные'); ?>
    <span class="required">*</span>
    <?php echo Yii::t('timeline', 'обязательны.'); ?>
</div>

<?php echo $form->errorSummary($model); ?>
<?php echo $form->errorSummary($user); ?>
<div class="row">
<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
    <div class="row">
        <div class="col-sm-12">



<?php /*echo $form->textFieldGroup($user,'nick_name'); */?>
<?php if (!$this->module->generateNickName) : ?>

            <?= $form->textFieldGroup($user, 'nick_name'); ?>

<?php endif; ?>

        <div class="form-group">
            <?php echo $form->labelEx($user,'phone',['class' => 'control-label col-sm-3']); ?>
            <div class="col-sm-9">
                <?php $this->widget(
                    'CMaskedTextField',
                    [
                        'model' => $user,
                        'attribute' => 'phone',
                        'mask' => $this->module->phoneMask,
                        'placeholder' => '*',
                        'htmlOptions' => [
                            'class' => 'form-control'
                        ]
                    ]
                ); ?>
                <?php echo $form->error($user,'phone'); ?>
            </div>
        </div>


<?php if (!$this->module->generateEmail) : ?>

            <?php echo $form->textFieldGroup($user, 'email'); ?>

<?php endif; ?>



        <?php echo $form->textFieldGroup($user, 'last_name'); ?>

        <?php echo $form->textFieldGroup($user, 'first_name'); ?>


        <?php echo $form->textFieldGroup($user, 'middle_name'); ?>

        <?php echo $form->dropDownListGroup(
            $user,
            'gender',
            [
                'widgetOptions' => [
                    'data' => $user->getGendersList(),
                ],
            ]
        ); ?>

        <?php echo $form->datePickerGroup(
            $user,
            'birth_date',
            [
                'widgetOptions' => [
                    'options' => [
                        'format'      => 'yyyy-mm-dd',
                        'weekStart'   => 1,
                        'autoclose'   => true,
                        'orientation' => 'auto right',
                        'startView'   => 2,
                    ],
                    'htmlOptions' => [
                        'class' => ''
                    ],
                ],
                'prepend'       => '<i class="fa fa-calendar"></i>',
            ]
        );
        ?>

        <?php echo $form->textAreaGroup(
            $user,
            'about',
            array(
                'wrapperHtmlOptions' => array(
                    'class' => 'col-sm-9',
                ),
                'widgetOptions' => array(
                    'htmlOptions' => array(
                        'rows' => 5
                    ),
                )
            )
        ); ?>

<?php echo $form->hiddenField($user,'status',['value'=>User::STATUS_ACTIVE]);?>
<?php echo $form->hiddenField($user,'email_confirm',['value'=>User::EMAIL_CONFIRM_YES]);?>
<?php /*echo $form->hiddenField($user,'access_level',['value'=>User::ACCESS_LEVEL_USER]);*/?>



                    </div>
                </div>
            </div>
<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
    <div class="row">
        <div class="col-sm-12">




                    <?php echo $form->dropDownListGroup(
                        $model,
                        'visit',
                        [
                            'widgetOptions' => [
                                'data'        => $model->getVisitList(),
                                'htmlOptions' => [
                                    'class'               => 'popover-help',
                                    'data-original-title' => $model->getAttributeLabel('visit'),
                                    'data-content'        => $model->getAttributeDescription('visit'),
                                    'options'=>
                                        array(
                                            '1'=>array('selected'=>'selected')
                                        )
                                ],
                            ],
                        ]
                    ); ?>

                    <?php echo $form->dropDownListGroup(
                        $model,
                        'notified',
                        [
                            'widgetOptions' => [
                                'data'        => $model->getNotifiedList(),
                                'htmlOptions' => [
                                    'class'               => 'popover-help',
                                    'data-original-title' => $model->getAttributeLabel('notified'),
                                    'data-content'        => $model->getAttributeDescription('notified')
                                ],
                            ],
                        ]
                    ); ?>


                    <?php /*echo $form->dropDownListGroup(
                        $model,
                        'status',
                        [
                            'widgetOptions' => [
                                'data'        => $model->getStatusList(),
                                'htmlOptions' => [
                                    'class'               => 'popover-help',
                                    'data-original-title' => $model->getAttributeLabel('status'),
                                    'data-content'        => $model->getAttributeDescription('status')
                                ],
                            ],
                        ]
                    ); */?>

                    <?php echo $form->textAreaGroup(
                        $model,
                        'comment',
                        array(
                            'widgetOptions' => array(
                                'htmlOptions' => array(
                                    'class' => 'popover-help',
                                    'rows' => 6,
                                    'cols' => 50,
                                    'data-original-title' => $model->getAttributeLabel('comment'),
                                    'data-content' => $model->getAttributeDescription('comment')
                                )
                            )
                        )
                    ); ?>




        </div>
    </div>
</div>
</div>
<?php echo $form->hiddenField($model,'doctor_id');?>
<?php echo $form->hiddenField($model,'start_time');?>
<?php echo $form->hiddenField($model,'end_time');?>

<?php $this->widget(
    'bootstrap.widgets.TbButton',
    [
        'buttonType' => 'submit',
        'context'    => ($model->getScenario() === 'update' ?  'success' : 'primary'),
        'htmlOptions'=> array('name' => 'submit-type', 'value' => 'with-new-user'),
        'label'      => ($model->getScenario() === 'update' ? Yii::t('timeline', 'Добавить пациента и обновить запись на прием') : Yii::t('timeline', 'Добавить пациента и записать на прием')),
    ]
); ?>


<?php $this->endWidget();
/*CVarDumper::dump($user->scenario);
CVarDumper::dump($user->email);
CVarDumper::dump(Yii::app()->getBaseUrl(true));*/
?>
