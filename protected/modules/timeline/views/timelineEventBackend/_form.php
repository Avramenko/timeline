<?php
/**
 * Отображение для _form:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 *
 *   @var $model TimelineEvent
 *   @var $form TbActiveForm
 *   @var $this TimelineEventBackendController
 **/
//Yii::app()->getClientScript()->registerScriptFile('https://cdnjs.cloudflare.com/ajax/libs/placeholders/4.0.1/placeholders.jquery.min.js',CClientScript::POS_END);
/*Yii::app()->clientScript->registerScript('UpdateEvent',
<<<JS
    jQuery(document).on('click','a.update-event-button', function () {
        console.log($(this).data('url'));
        $.ajax({
            url: $(this).data('url'),
            dataType: "html",
            success: function(data){
                jQuery("#view-event-modal .modal-body").html(data);
            },
            error : function(data,status){
                console.log(data);
            }
        });
        return false;
    });
JS
    ,CClientScript::POS_END);*/

$form = $this->beginWidget(
    'bootstrap.widgets.TbActiveForm', array(
        'id'                     => 'timeline-event-form',
        'enableAjaxValidation'   => true,
        'enableClientValidation' => true,
        'type'                   => 'horizontal',
        'htmlOptions'            => array('class' => 'well'),
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'afterValidate'=>'js:function(form,data,hasError){
                                    if(!hasError){
                                        $.ajax({
                                            type:"POST",
                                            url:"'.($model->getScenario() === 'update' ? CHtml::normalizeUrl(array("/backend/timeline/timelineEvent/update/".$model->id)) : CHtml::normalizeUrl(array("TimelineEventBackend/create"))).'",
                                            data:form.serialize(),
                                            dataType : "json",
                                            success: function(d,status){
                                                console.log(status);
                                                //console.log(d.status);

                                                /*if(d.status){
                                                    console.log("Равны");
                                                }*/
                                                //$("#patient-registration-modal").modal("hide");
                                                //console.log(d);
                                                //$("#calendar").fullCalendar( "addEventSource", [d] );
                                                $("#calendar").fullCalendar("refetchEvents");
                                                $("#my-modal").modal("hide");
                                                //$("#my-modal").modal("hide");
                                            }
                                        });
                                    }
                                    return false;
                                }'
        ),
    )
);
?>

<div class="alert alert-info">
    <?php echo Yii::t('timeline', 'Поля, отмеченные'); ?>
    <span class="required">*</span>
    <?php echo Yii::t('timeline', 'обязательны.'); ?>
</div>


<?php echo $form->errorSummary($model); ?>
<?php echo $form->hiddenField($model,'doctor_id');?>
<?php echo $form->hiddenField($model,'start_time');?>
<?php echo $form->hiddenField($model,'end_time');?>


<div class="row">
    <div class="col-md-6 col-lg-6">
        <div class="row">
            <div class="col-sm-12">




            <?php echo $form->select2Group(
                $model,
                'patient_id',
                [
                    'widgetOptions' => [
                        //'data' => ['id'=>$model->patient_id, 'text' => $model->patient->getFullName()],
                        'asDropDownList' => false,
                        'options' => [
                            'minimumInputLength'=>'3',
                            //'placeholder' => ($model->getScenario() === 'update' ? $model->patient->getFullName() : 'Фамилия'),
                            'placeholder' => 'Фамилия',
                            //'allowClear' => false,
                            //'value' => ['id'=>$model->patient_id, 'text'=>'test'],
                            //'val' => ($model->getScenario() === 'update' ? [$model->patient_id => $model->patient->getFullName()] : []),
                            'ajax'       => array(
                                'url'       => Yii::app()->controller->createUrl('/backend/timeline/patient/select2'),
                                'dataType'  => 'json',
                                'data'      => 'js:function(term, page) { return {q: term }; }',
                                'results'   => 'js:function(data) {
                                    if(!data.length){
                                        console.log("no matches");
                                        //jQuery("#select2-results-2").append("<li>").html("Добавить нового пациента");
                                        }
                                    return {results: data};
                                 }',
                            ),
                        ],
                        'htmlOptions' => [
                            'data-placeholder' => ($model->getScenario() === 'update' ? $model->patient->getFullName() : 'Фамилия')
                        ]
                    ],

                ]
            ); ?>


            </div>
        </div>
    </div>
    <div class="col-md-6 col-lg-6">
        <div class="row">
            <div class="col-sm-12">




            <?php echo $form->dropDownListGroup(
                $model,
                'visit',
                [
                    'widgetOptions' => [
                        'data'        => $model->getVisitList(),
                        'htmlOptions' => [
                            'class'               => 'popover-help',
                            'data-original-title' => $model->getAttributeLabel('visit'),
                            'data-content'        => $model->getAttributeDescription('visit')
                        ],
                    ],
                ]
            ); ?>


            <?php if($model->getScenario() === 'update'){?>
                <?php echo $form->dropDownListGroup(
                    $model,
                    'notified',
                    [
                        'widgetOptions' => [
                            'data'        => $model->getNotifiedList(),
                            'htmlOptions' => [
                                'class'               => 'popover-help',
                                'data-original-title' => $model->getAttributeLabel('notified'),
                                'data-content'        => $model->getAttributeDescription('notified')
                            ],
                        ],
                    ]
                ); ?>
            <?php } ?>


            <?php /*echo $form->dropDownListGroup(
                $model,
                'status',
                [
                    'widgetOptions' => [
                        'data'        => $model->getStatusList(),
                        'htmlOptions' => [
                            'class'               => 'popover-help',
                            'data-original-title' => $model->getAttributeLabel('status'),
                            'data-content'        => $model->getAttributeDescription('status')
                        ],
                    ],
                ]
            ); */?>


            <?php echo $form->textAreaGroup(
                $model,
                'comment',
                array(
                    'widgetOptions' => array(
                        'htmlOptions' => array(
                            'class' => 'popover-help',
                            'rows' => 6,
                            'cols' => 50,
                            'data-original-title' => $model->getAttributeLabel('comment'),
                            'data-content' => $model->getAttributeDescription('comment')
                        )
                    )
                )
            ); ?>

</div>
            </div>
        </div>
    </div>

<?php if($model->scenario === 'insert'){?>
    <?php
    $this->widget(
        'bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'context'    => 'primary',
            //'label'      => Yii::t('timeline', 'Сохранить Событие'),
            'label'      => Yii::t('timeline', 'Записать на прием'),
        )
    ); ?>
<?php }else{?>
    <?php
    $this->widget(
        'bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'context'    => 'success',
            'htmlOptions'=> array('name' => 'submit-type', 'value' => 'update'),
            //'label'      => Yii::t('timeline', 'Сохранить Событие'),
            'label'      => Yii::t('timeline', 'Обновить запись на прием'),
        )
    ); ?>
<?php }?>
<?php $this->endWidget(); ?>


