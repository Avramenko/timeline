<?php
/**
 * Отображение для view:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
    $this->breadcrumbs = array(
        Yii::app()->getModule('timeline')->getCategory() => array(),
        Yii::t('timeline', 'Сообщения') => array('/timeline/timeline/TimelineMessageBackend/index'),
        $model->id,
    );

    $this->pageTitle = Yii::t('timeline', 'Сообщения - просмотр');

    $this->menu = array(
        array('icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('timeline', 'Управление Сообщениями'), 'url' => array('/timeline/timeline/TimelineMessageBackend/index')),
        array('icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('timeline', 'Добавить Сообщение'), 'url' => array('/timeline/timeline/TimelineMessageBackend/create')),
        array('label' => Yii::t('timeline', 'Сообщение') . ' «' . mb_substr($model->id, 0, 32) . '»'),
        array('icon' => 'fa fa-fw fa-pencil', 'label' => Yii::t('timeline', 'Редактирование Сообщение'), 'url' => array(
            '/timeline/timeline/TimelineMessageBackend/update',
            'id' => $model->id
        )),
        array('icon' => 'fa fa-fw fa-eye', 'label' => Yii::t('timeline', 'Просмотреть Сообщение'), 'url' => array(
            '/timeline/timeline/TimelineMessageBackend/view',
            'id' => $model->id
        )),
        array('icon' => 'fa fa-fw fa-trash-o', 'label' => Yii::t('timeline', 'Удалить Сообщение'), 'url' => '#', 'linkOptions' => array(
            'submit' => array('/timeline/timeline/TimelineMessageBackend/delete', 'id' => $model->id),
            'confirm' => Yii::t('timeline', 'Вы уверены, что хотите удалить Сообщение?'),
            'csrf' => true,
        )),
    );
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('timeline', 'Просмотр') . ' ' . Yii::t('timeline', 'Сообщение'); ?>        <br/>
        <small>&laquo;<?php echo $model->id; ?>&raquo;</small>
    </h1>
</div>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
'data'       => $model,
'attributes' => array(
        'id',
        'type',
        'mes',
        'sender',
        'time',
        'query',
        'cost',
        'cnt',
        'error',
        'err',
        'status',
        'last_date',
        'create_time',
        'update_time',
),
)); ?>
