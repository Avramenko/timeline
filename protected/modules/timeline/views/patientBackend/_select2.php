<?php
/**
 * Created by PhpStorm.
 * User: pebo
 * Date: 30.12.15
 * Time: 21:31
 * отображаем json для select2
 */

$json = [];
foreach($users as $k => $user)
{
    $json[] = [
        'id' => $user['id'],
        'text' => $user['last_name'] .
            ' '.$user['first_name'] .
            ' '.$user['middle_name'] .
            ' ('.substr($user['phone'], 0,6).'...'.substr($user['phone'], -2).')'
    ];
}
echo CJSON::encode($json);