<?php
$form = $this->beginWidget(
    'bootstrap.widgets.TbActiveForm',
    [
        'id'                     => 'patient-registration-form',
        'enableAjaxValidation'   => true,
        'enableClientValidation' => true,
        //'type'                   => 'vertical',
        'type'                   => 'horizontal',
        'htmlOptions'            => ['class' => 'well'],
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'afterValidate'=>'js:function(form,data,hasError){
                                    if(!hasError){
                                        $.ajax({
                                            type:"POST",
                                            url:"'.CHtml::normalizeUrl(array("patientBackend/create")).'",
                                            data:form.serialize(),
                                            dataType : "json",
                                            success: function(d,status){
                                                if(d.status){
                                                    //$("#calendar").fullCalendar("refetchEvents");
                                                    bootbox.alert("Новый пациент "+d.model.last_name+" "+d.model.first_name+" "+d.model.middle_name+" успешно создан! Для продолжения нажмите кнопку \'ОК\'")
                                                    $("#my-modal").modal("hide");

                                                }
                                            }
                                        });
                                    }
                                }'
        ),
    ]
);
?>

<div class="alert alert-info">
    <?php echo Yii::t('timeline', 'Поля, отмеченные'); ?>
    <span class="required">*</span>
    <?php echo Yii::t('timeline', 'обязательны.'); ?>
</div>

<?php echo $form->errorSummary($model); ?>


<?php /*echo $form->textFieldGroup($model,'nick_name'); */?>
<?php if (!$this->module->generateNickName) : ?>
    <div class='row'>
        <div class="col-sm-12">
            <?= $form->textFieldGroup($model, 'nick_name'); ?>
        </div>
    </div>
<?php endif; ?>
<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            <?php echo $form->labelEx($model,'phone',['class' => 'control-label col-sm-3']); ?>
            <div class="col-sm-9">
                <?php $this->widget(
                    'CMaskedTextField',
                    [
                        'model' => $model,
                        'attribute' => 'phone',
                        'mask' => $this->module->phoneMask,
                        'placeholder' => '*',
                        'htmlOptions' => [
                            'class' => 'form-control'
                        ]
                    ]
                ); ?>
                <?php echo $form->error($model,'phone'); ?>
            </div>
        </div>
    </div>
</div>

<?php if (!$this->module->generateEmail) : ?>
    <div class='row'>
        <div class="col-sm-12">
            <?php echo $form->textFieldGroup($model, 'email'); ?>
        </div>
    </div>
<?php endif; ?>

<div class="row">
    <div class="col-sm-12">
        <?php /*echo $form->textFieldGroup($model, 'email'); */?>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <?php echo $form->textFieldGroup($model, 'last_name'); ?>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <?php echo $form->textFieldGroup($model, 'first_name'); ?>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <?php echo $form->textFieldGroup($model, 'middle_name'); ?>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <?php echo $form->dropDownListGroup(
            $model,
            'gender',
            [
                'widgetOptions' => [
                    'data' => $model->getGendersList(),
                ],
            ]
        ); ?>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <?php echo $form->datePickerGroup(
            $model,
            'birth_date',
            [
                'widgetOptions' => [
                    'options' => [
                        'format'      => 'yyyy-mm-dd',
                        'weekStart'   => 1,
                        'autoclose'   => true,
                        'orientation' => 'auto right',
                        'startView'   => 2,
                    ],
                    'htmlOptions' => [
                        'class' => ''
                    ],
                ],
                'prepend'       => '<i class="fa fa-calendar"></i>',
            ]
        );
        ?>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <?php echo $form->textAreaGroup(
            $model,
            'about',
            array(
                'wrapperHtmlOptions' => array(
                    'class' => 'col-sm-9',
                ),
                'widgetOptions' => array(
                    'htmlOptions' => array(
                        'rows' => 5
                    ),
                )
            )
        ); ?>
    </div>
</div>
<?php echo $form->hiddenField($model,'status',['value'=>User::STATUS_ACTIVE]);?>
<?php echo $form->hiddenField($model,'email_confirm',['value'=>User::EMAIL_CONFIRM_YES]);?>
<?php /*echo $form->hiddenField($model,'access_level',['value'=>User::ACCESS_LEVEL_USER]);*/?>
<?php $this->widget(
    'bootstrap.widgets.TbButton',
    [
        'buttonType' => 'submit',
        'context'    => 'primary',
        'label'      => Yii::t('TimelineModule.timeline', 'Добавить новго пациента'),
    ]
); ?>


<?php $this->endWidget();
?>
