<?php
/**
 * Отображение для _form:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 *
 *   @var $model TimelineDoctor
 *   @var $form TbActiveForm
 *   @var $this TimelineDoctorBackendController
 **/
$form = $this->beginWidget(
    'bootstrap.widgets.TbActiveForm', array(
        'id'                     => 'timeline-doctor-form',
        'enableAjaxValidation'   => false,
        'enableClientValidation' => true,
        'htmlOptions'            => array('class' => 'well'),
    )
);
?>

<div class="alert alert-info">
    <?php echo Yii::t('timeline', 'Поля, отмеченные'); ?>
    <span class="required">*</span>
    <?php echo Yii::t('timeline', 'обязательны.'); ?>
</div>

<?php echo $form->errorSummary($model); ?>

    <div class="row">
        <div class="col-sm-7">
            <?php echo $form->dropDownListGroup(
                $model,
                'status',
                [
                    'widgetOptions' => [
                        'data'        => $model->getStatusList(),
                        'htmlOptions' => [
                            'class'               => 'popover-help',
                            'data-original-title' => $model->getAttributeLabel('status'),
                            'data-content'        => $model->getAttributeDescription('status')
                        ],
                    ],
                ]
            ); ?>
        </div>

    </div>

    <div class="row">
        <div class="col-sm-7">
            <?php echo $form->select2Group(
                $model,
                'user_id',
                [
                    'widgetOptions' => [
                        /*'htmlOptions' => [
                            'id' => 'doctor-select',
                            'class' => 'col-sm-5'
                        ],*/
                        //'data' => ['' => '---'] + CHtml::listData(User::model()->getList(), 'id', 'fullName'),
                        'asDropDownList' => false,
                        'options' => [
                            'minimumInputLength'=>'3',
                            //'width'      => '348px',
                            'placeholder'=> 'ФИО',
                            'allowClear' => TRUE,

                            'ajax'       => array(
                                'url'       => Yii::app()->controller->createUrl('/backend/timeline/patient/select2'),
                                'dataType'  => 'json',
                                'data'      => 'js:function(term, page) { return {q: term }; }',
                                'results'   => 'js:function(data) { return {results: data}; }',
                            ),
                        ]
                    ],

                ]
            ); ?>
        </div>

    </div>
    <div class="row">
        <div class="col-sm-7">
            <?php echo $form->textFieldGroup($model, 'position', array('widgetOptions' => array('htmlOptions' => array('class' => 'popover-help', 'data-original-title' => $model->getAttributeLabel('position'), 'data-content' => $model->getAttributeDescription('position'))))); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-7">
            <?php echo $form->textAreaGroup($model, 'description', array('widgetOptions' => array('htmlOptions' => array('class' => 'popover-help', 'rows' => 6, 'cols' => 50, 'data-original-title' => $model->getAttributeLabel('description'), 'data-content' => $model->getAttributeDescription('description'))))); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-7">

            <?php echo $form->colorpickerGroup(
                $model,
                'color',
                array(
                    'wrapperHtmlOptions' => array(
                        'class' => 'col-sm-5'
                    ),
                    'hint' => 'Выберите цвет для врача',
                )
            ); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-7">
            <?php echo $form->textFieldGroup(
                $model,
                'min_time',
                array('widgetOptions' => array(
                    'htmlOptions' => array(
                        'class' => 'popover-help',
                        'data-original-title' => $model->getAttributeLabel('min_time'),
                        'data-content' => $model->getAttributeDescription('min_time')
                    )
                ))
            ); ?>
            <?php /*echo $form->datePickerGroup(
                $model,
                'birth_date',
                [
                    'widgetOptions' => [
                        'options' => [
                            'format' => 'yyyy-mm-dd',
                            'weekStart' => 1,
                            'autoclose' => true,
                            'orientation' => 'auto right',
                            'startView' => 2,
                        ],
                    ],
                    'prepend' => '<i class="fa fa-calendar"></i>',
                ]
            ); */?>
        </div>
    </div>
    <?php
    $this->widget(
        'bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'context'    => 'primary',
            'label'      => Yii::t('timeline', 'Сохранить Доктора и продолжить'),
        )
    ); ?>
    <?php
    $this->widget(
        'bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'htmlOptions'=> array('name' => 'submit-type', 'value' => 'index'),
            'label'      => Yii::t('timeline', 'Сохранить Доктора и закрыть'),
        )
    ); ?>

<?php $this->endWidget(); ?>