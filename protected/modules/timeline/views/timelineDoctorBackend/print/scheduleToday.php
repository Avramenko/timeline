<?php
/**
 * scheduleToday view for print Doctors Schedule for today
 */
echo Yii::app()->getRequest()->baseUrl;
$this->layout='print';
if($model){
    foreach($model as $doctor) {
        echo $this->renderPartial('print/doctorEventScheduleTable', array(
            'model' => $doctor,
            'start_time' => $start_time,
            'end_time' => $end_time,
        ));
    }
}
