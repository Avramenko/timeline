<?php
/**
 * doctorEventSheduleTable это view для отображения расписания для конкретного врача
 * $today date текущая дата
 *
 */
//echo CVarDumper::dump($model);
//$today = new DateTime('today');
//echo $today->format('Y-m-d');
//$tomorrow = new DateTime('tomorrow');
//echo $tomorrow->format('Y-m-d');

?>
<?php if($model->events){
    //$date = new CDateFormatter('ru')
    $DoctorScheduleTime = ($start_time->diff($end_time)->days === 1 ? $start_time->format('d.m.Y') : $start_time->format('d.m.Y') .' - '.$end_time->format('d.m.Y'));
    ?>


    <table class="table table-bordered table-condensed">
        <thead>
            <tr>
                <th colspan="5" style="text-align: center;"><b><?php echo $model->user->fullName?></b> (<?php echo $DoctorScheduleTime?>)</th>
            </tr>
            <tr>
                <th>Время</th>
                <th>ФИО пациента</th>
                <th>Комментарий</th>
                <th>Источник</th>
                <th>1/2</th>
            </tr>
        </thead>
        <tbody>
        <?php
        foreach($model->events as $event){
            $time = new DateTime($event->start_time);
        ?>
            <tr>
                <td><?php echo $time->format('H:i');?></td>
                <td><?php echo $event->patient->fullName;?></td>
                <td><?php echo $event->comment;?></td>
                <td><?php echo $event->patient->about;?></td>
                <td><?php echo $event->visit;?></td>
            </tr>
        <?php }?>
        </tbody>
    </table>
    <hr>
<?php } ?>