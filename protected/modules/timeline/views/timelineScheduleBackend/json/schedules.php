<?php
header('Content-Type: application/json');
$json = [];
foreach($schedules as $schedule ){
    $json[] = [
        'id' => (!$schedule->status ? 'notAvailable' : 'available'),
        'start'=> $schedule->start_time,
        'end'=> $schedule->end_time,
        'rendering'=> 'background',
        'color'=> $schedule->getStatusColor(),
        'resourceId' => $schedule->doctor_id,
        'overlap' => (!$schedule->status ? false : true),
    ];
}
echo CJSON::encode($json);