<?php
/**
 * Отображение для _form:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 *
 *   @var $model TimelineSchedule
 *   @var $form TbActiveForm
 *   @var $this TimelineScheduleBackendController
 **/
$url = ($model->scenario === 'insert')  ? CHtml::normalizeUrl(array("timelineScheduleBackend/create")) : CHtml::normalizeUrl(array("/backend/timeline/timelineSchedule/update/".$model->id));
$form = $this->beginWidget(
    'bootstrap.widgets.TbActiveForm', array(
        'id'                     => 'timeline-schedule-form',
        'enableAjaxValidation'   => true,
        'enableClientValidation' => true,
        'htmlOptions'            => array('class' => 'well'),
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'afterValidate'=>'js:function(form,data,hasError){
                                    if(!hasError){
                                        $.ajax({
                                            type:"POST",
                                            url:"'.$url .'",
                                            data:form.serialize(),
                                            dataType : "json",
                                            success: function(data,status){
                                                console.log(data);
                                                console.log(status);

                                                //$("#calendar").fullCalendar( "removeEvents", ['.$model->id.'] );
                                                //$("#calendar").fullCalendar( "addEventSource", [data] );
                                                $("#calendar").fullCalendar("refetchEvents");
                                                $("#schedule-modal").modal("hide");
                                            }
                                        });
                                    }
                                }'
        ),
    )
);
?>

<div class="alert alert-info">
    <?php echo Yii::t('timeline', 'Поля, отмеченные'); ?>
    <span class="required">*</span>
    <?php echo Yii::t('timeline', 'обязательны.'); ?>
</div>

<?php echo $form->errorSummary($model); ?>

    <?php echo $form->hiddenField($model,'doctor_id')?>
    <?php echo $form->hiddenField($model,'start_time')?>
    <?php echo $form->hiddenField($model,'end_time')?>
    <div class="row">
        <div class="col-sm-7">
            <?php echo $form->textAreaGroup($model, 'comment', array('widgetOptions' => array('htmlOptions' => array('class' => 'popover-help', 'rows' => 6, 'cols' => 50, 'data-original-title' => $model->getAttributeLabel('comment'), 'data-content' => $model->getAttributeDescription('comment'))))); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-7">

            <?php echo $form->dropDownListGroup(
                $model,
                'status',
                [
                    'widgetOptions' => [
                        'data'        => $model->getStatusList(),
                        'htmlOptions' => [
                            'class'               => 'popover-help',
                            'data-original-title' => $model->getAttributeLabel('status'),
                            'data-content'        => $model->getAttributeDescription('status')
                        ],
                    ],
                ]
            ); ?>
        </div>
    </div>
    <?php if($model->scenario === 'insert'){?>
        <?php
        $this->widget(
            'bootstrap.widgets.TbButton', array(
                'buttonType' => 'submit',
                'context'    => 'primary',
                'label'      => Yii::t('timeline', 'Сохранить График и продолжить'),
            )
        ); ?>
        <?php
        $this->widget(
            'bootstrap.widgets.TbButton', array(
                'buttonType' => 'submit',
                'htmlOptions'=> array('name' => 'submit-type', 'value' => 'index'),
                'label'      => Yii::t('timeline', 'Сохранить График и закрыть'),
            )
        ); ?>
    <?php }else{ ?>
        <?php
        $this->widget(
            'bootstrap.widgets.TbButton', array(
                'buttonType' => 'submit',
                'context'    => 'primary',
                'htmlOptions'=> array('name' => 'submit-type', 'value' => 'update'),
                'label'      => Yii::t('timeline', 'Сохранить График'),
            )
        ); ?>
    <?php } ?>


<?php $this->endWidget(); ?>