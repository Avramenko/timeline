<?php
/**
 * Отображение для create:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
    $this->breadcrumbs = array(
        Yii::app()->getModule('timeline')->getCategory() => array(),
        Yii::t('timeline', 'Графики') => array('/backend/timeline/timelineSchedule/index'),
        Yii::t('timeline', 'Добавление'),
    );

    $this->pageTitle = Yii::t('timeline', 'Графики - добавление');

    $this->menu = array(
        array('icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('timeline', 'Управление Графиками'), 'url' => array('/backend/timeline/timelineSchedule/index')),
        array('icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('timeline', 'Добавить График'), 'url' => array('/backend/timeline/timelineSchedule/create')),
    );
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('timeline', 'Графики'); ?>
        <small><?php echo Yii::t('timeline', 'добавление'); ?></small>
    </h1>
</div>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>