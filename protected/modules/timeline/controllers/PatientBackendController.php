<?php
/**
 * Класс PatientBackendController:
 *
 *   @category YupeController
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/




class PatientBackendController extends yupe\components\controllers\BackController
{
    /**
     * Создает новую модель User.
     * Если создание прошло успешно - перенаправляет на просмотр.
     *
     * @throws CException
     * @return void
     */
    public function actionCreate()
    {
        //$model = new PatientRegistrationForm();
        $model = new User('patient');
        //Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (($data = Yii::app()->getRequest()->getPost('User')) !== null) {

            $model->setAttributes($data);

            $model->setAttributes(
                [
                    'hash' => Yii::app()->userManager->hasher->hashPassword(
                        Yii::app()->userManager->hasher->generateRandomPassword()
                    ),
                ]
            );


            if ($model->save()) {

                header('Content-type: text/javascript');
                echo CJSON::encode(array(
                    'status'=>true,
                    'scenario' => $model->scenario,
                    'validate' => $model->validate(),
                    'model' => $model,
                ));
                Yii::app()->end();

            }
        }
        $this->renderPartial('_form', array('model' => $model),false,true);
    }

    /**
     * Производит AJAX-валидацию
     *
     * @param User model, которую необходимо валидировать
     *
     * @return void
     */
    protected function performAjaxValidation(User $model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'patient-registration-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * Поиск пациентов по первым буквам фамилии
     * Дополнить поиск не только по фамилии но и по имени отчеству
     * @throws CException
     */
    public function actionSelect2()
    {
        $users = Yii::app()->db->createCommand()
            ->select()
            ->from('{{user_user}}')
            ->where(array('like', 'last_name', ''.trim($_GET['q']).'%') )
            ->order('last_name asc, first_name asc, middle_name asc')
            ->queryAll()
        ;
        $this->renderPartial('_select2', compact('users'), false, false);
    }

    /**
     * Возвращает модель по указанному идентификатору
     * Если модель не будет найдена - возникнет HTTP-исключение.
     *
     * @param integer идентификатор нужной модели
     *
     * @return void
     * @throws CHttpException 404
     */
    public function loadModel($id)
    {
        $model = User::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, Yii::t('timeline', 'Запрошенная страница не найдена.'));

        return $model;
    }
}