<?php
/**
 *
 * Файл конфигурации модуля timeline
 *
 * @author afclinic team <maxim.avramenko@gmail.com>
 * @link http://yupe.ru
 * @copyright 2009-2015 Goodly
 * @package yupe.modules.timeline.install
 * @license  BSD https://raw.github.com/yupe/yupe/master/LICENSE
 *
 */
return [
    'module'   => [
        'class'  => 'application.modules.timeline.TimelineModule',
        'defaultController' => 'timelineDoctorBackend',
    ],
    'import'    => [
        'application.modules.timeline.models.*',
        'application.modules.timeline.forms.*',
    ],
    'commandMap' => [
        'send-notification' => [
            'class' => 'application.modules.timeline.commands.SendNotificationCommand'
        ],
        'upload-patient-csv' => [
            'class' => 'application.modules.timeline.commands.UploadPatientsCommand'
        ]
    ],
    // обязательно явно прописываем все публичне урл-адреса, так как у нас CUrlManager::useStrictParsing === true
    'rules'     => [

    ],
    'component' => [

    ]
];