<?php

/**
 * Разбираем начиная с последней записи к первой.
 * Без телефонов не добавляем в базу.
 * Проверяем сначала телефон, если есть такой телефон то переходим к разбору следующей строки из csv и ничего не добавляем,
 * Если нет такого телефона значит может быть сменился телефон и проверяем фамилию,
 * Если нет фамилии то добавляем пациента в базу,
 * Если фамилия есть то делаем еще один запрос и проверяем фамилию имя и отчество,
 * Если совпадений нет то добавляем пациента в базу.
 *
 * Class UploadPatientsCommand
 */
use League\Csv\Reader;

class UploadPatientsCommand extends CConsoleCommand
{

    public function run($args)
    {
        echo "Загружаем архив пациентов\n";
        $data = [];
        $files = scandir(Yii::getPathOfAlias('application') . '/../temp/csv');
        unset($files[0]);
        unset($files[1]);
        foreach ($files as $file) {


            //echo CVarDumper::dump($files);
            $csv = Reader::createFromPath(Yii::getPathOfAlias('application') . '/../temp/csv/'.$file, 'r');
            //$csv->setOffset(1);
            $csv->setDelimiter(';');
            $ascending = $csv->fetchAll();
            $descending = array_reverse($ascending);
            //CVarDumper::dump($descending);

            //Yii::app()->end();

            //$csv = Reader::createFromPath(Yii::getPathOfAlias('application') . '/../temp/temp.csv', 'r');
            //$csv->setOffset(1);
            //$csv->setDelimiter(';');
            //$ascending = $csv->fetchAll();
            //$descending = array_reverse($ascending);
            // Достаем из базы события завтрашнего дня и отправляем каждому напоминание о визите к врачю
            //CVarDumper::dump($descending);
            //$user = User::model();
            $i = 0;
            foreach ($descending as $row) {

                $user = [];
                $phones = [];
                $data['User'] = [];
                //each row member will be uppercased
                //CVarDumper::dump($row);
                // достаем "ФИО по отдельности", затем "телефон" и "кто направил"
                $user = explode(' ', $row[0], 3);

                // как то нужно дописать номера до общего стандарта и не запаганить список контактов :)

                $data['User']['last_name'] = $user[0];
                if (isset($user[1])) {
                    $data['User']['first_name'] = $user[1];
                }
                if (isset($user[2])) {
                    $data['User']['middle_name'] = $user[2];
                }
                if (isset($row[1])) {
                    $phones = explode(',', $row[1], 2);
                    $data['User']['phone'] = $this->cleanPhoneNumber($phones[0]);

                    if (isset($phones[1])) {
                        $data['User']['about'] = 'Доп. номер для связи: ' . $this->cleanPhoneNumber($phones[1]);
                    }
                }
                if (isset($row[2])) {
                    if (isset($phones[1])) {
                        $data['User']['about'] = $row[2] . '. ' . $data['User']['about'];
                    } else {
                        $data['User']['about'] = $row[2];
                    }
                }


                // Получили массив с данными для внесения в базу
                // Если есть телефон то найдем совпадения телефона в базе
                if (array_key_exists('phone', $data['User'])) {
                    $model = User::model()->find([
                        'condition' => 'phone =:phone',
                        'params' => [
                            'phone' => $data['User']['phone']
                        ]
                    ]);
                    // Если есть такой телефон то ничего не делаем, данные по телефону - актуальны
                    if (!$model) {
                        // если нет такого телефона то пробуем найти по фамилии и имени
                        if (
                            array_key_exists('last_name', $data['User'])
                            && array_key_exists('first_name', $data['User'])
                            && array_key_exists('middle_name', $data['User'])
                            && strlen($data['User']['last_name'])
                            && strlen($data['User']['first_name'])
                            && strlen($data['User']['middle_name'])
                        ) {
                            $model = User::model()->find([
                                'condition' => 'last_name =:last_name AND first_name =:first_name AND middle_name =:middle_name',
                                'params' => [
                                    'last_name' => $data['User']['last_name'],
                                    'first_name' => $data['User']['first_name'],
                                    'middle_name' => $data['User']['middle_name'],
                                ]
                            ]);
                            if (!$model) {
                                // получается если нет совпадений по телефону и нет совпадений по Фамилии и Имени то можем добавлять в базу пациента.
                                $model = new User();
                                $model->setAttributes($data['User']);

                                $model->setAttributes(
                                    [
                                        'hash' => Yii::app()->userManager->hasher->hashPassword(
                                            Yii::app()->userManager->hasher->generateRandomPassword()
                                        ),
                                    ]
                                );

                                if ($model->save()) {
                                    // Если получилось сохранить данные то выводим что то на экран
                                    //Yii::log('TRUE - Пациент: ID - '. $model->id. ': "' .$model->last_name . ' ' . $model->first_name . ' '.$model->middle_name.'" дабавлен', 'info');
                                    echo "\n";
                                    echo 'TRUE - Пациент: ID - ' . $model->id . ': "' . $model->last_name . ' ' . $model->first_name . ' ' . $model->middle_name . '" дабавлен';
                                    echo "\n";

                                } else {
                                    //Yii::log('FALSE -Пациент: "'.$model->id. ': "' .$data['User']['last_name'] . ' ' . $data['User']['first_name'] . ''.$data['User']['middle_name'].'" не дабавлен', 'error');
                                    // выводим на эран сообщение о том что не смогли сохранить данные пациента
                                    echo 'FALSE -Пациент: "' . $data['User']['last_name'] . ' ' . $data['User']['first_name'] . ' ' . $data['User']['middle_name'] . '" не дабавлен';
                                    echo "\n";
                                }
                            }

                        } elseif (
                            array_key_exists('last_name', $data['User'])
                            && array_key_exists('first_name', $data['User'])
                            && !array_key_exists('middle_name', $data['User'])
                            && strlen($data['User']['last_name'])
                            && strlen($data['User']['first_name'])
                        ) {
                            $model = User::model()->find([
                                'condition' => 'last_name =:last_name AND first_name =:first_name',
                                'params' => [
                                    'last_name' => $data['User']['last_name'],
                                    'first_name' => $data['User']['first_name'],

                                ]
                            ]);
                            $this->addPatient($model, $data);


                        }


                    }


                }
                //Yii::log('Закончили обработку строку - '. $i++ , 'info');
                echo 'Закончили обработку строку - ' . $i++ . "\n";


                //$model = new User('patient');

            }
            //CVarDumper::dump();
        }
    }

    public function addPatient($model, $data)
    {
        if(!$model ){
            // получается если нет совпадений по телефону и нет совпадений по Фамилии и Имени то можем добавлять в базу пациента.
            $model = new User();
            $model->setAttributes($data['User']);

            $model->setAttributes(
                [
                    'hash' => Yii::app()->userManager->hasher->hashPassword(
                        Yii::app()->userManager->hasher->generateRandomPassword()
                    ),
                ]
            );

            if ($model->save()) {
                // Если получилось сохранить данные то выводим что то на экран
                //Yii::log('TRUE - Пациент: ID - '. $model->id. ': "' .$model->last_name . ' ' . $model->first_name . '" дабавлен', 'info');
                echo "\n";
                echo 'TRUE - Пациент: ID - '. $model->id. ': "' .$model->last_name . ' ' . $model->first_name . '" дабавлен';
                echo "\n";

            }else{
                //Yii::log('FALSE -Пациент: "'.$model->id. ': "' .$data['User']['last_name'] . ' ' . $data['User']['first_name'] . '" не дабавлен', 'error');
                // выводим на эран сообщение о том что не смогли сохранить данные пациента
                echo 'FALSE -Пациент: "'.$data['User']['last_name'] . ' ' . $data['User']['first_name'] . '" не дабавлен';
                echo "\n";
            }
        }
    }
    public function cleanPhoneNumber($phone)
    {
        $phone = preg_replace("/[^0-9]/i","",(string)$phone);
        $length = strlen($phone);

        // разбираем телефон и приводим его к общему виду

        //if($length === 10){$phone = '8-812-'.$phone;}
        switch($length){
            case 7:
                $phone = '+7-812-'.$phone[0].$phone[1].$phone[2].'-'.$phone[3].$phone[4].'-'.$phone[5].$phone[6];
                break;
            case 10:
                $phone = '+7-'.$phone[0].$phone[1].$phone[2].'-'.$phone[3].$phone[4].$phone[5].'-'.$phone[6].$phone[7].'-'.$phone[8].$phone[9];
                break;
            case 11:
                $phone = '+7-'.$phone[1].$phone[2].$phone[3].'-'.$phone[4].$phone[5].$phone[6].'-'.$phone[7].$phone[8].'-'.$phone[9].$phone[10];
                break;
        }
        return $phone;
    }

}
